const wrapper = document.querySelector('.wrapper');
// // const formationlink = document.querySelector('.formation-link');
// const formateurlink = document.querySelector('.formateur-link');
// const materiellink = document.querySelector('.materiel-link');
const iconclose = document.querySelector('.icon-close');
const btnpopup = document.querySelector('.btn-popup');
const form = document.getElementById('form-id')
const idFormateur = document.getElementById('idFormateur')
const submitBtn = document.getElementById('btn-submit')

submitBtn.addEventListener('click', (e) => {
    e.preventDefault()
    console.log('Submit clicked !!')
    let isValid = true
    // do test if all fields valid
    // idFormateur.target.value.length > 0
    if(isValid){
        console.log('Submit clicked + code valid !!')
        // call php system.php code 
        form.onsubmit()
    }else{
        // display error to user
        console.log('Submit clicked + code not valid !!')
    }

})


// formationlink.addEventListener('click', ()=> {
//     wrapper.classList.add('active')
// });


// formateurlink.addEventListener('click', ()=> {
//     wrapper.classList.remove('active')
// });

// materiellink.addEventListener('click', ()=> {
//     wrapper.classList.add('active')
// });

// iconclose.addEventListener('click', ()=> {
//     wrapper.classList.remove('active-popup')
// });

// btnpopup.addEventListener('click', ()=> {
//     wrapper.classList.add('active-popup')
// });

function verifformateur(){
    document.getElementById("form-id").addEventListener("submit", function(event) {
        var idformateur = document.getElementById("idformateur").value;
        var nom = document.getElementById("nom").value;
        var prenom = document.getElementById("prenom").value;
        var adresse = document.getElementById("adresse").value;
        var experience = document.getElementById("experience").value;
        var disponible = document.getElementById("disponible");
        var dispo = document.getElementById("dispo");
        var idformation = document.getElementById("idformation").value;

        if (idformateur.length > 4 ) {
            event.preventDefault();
            alert("Le champ id du formateur ne doivent pas dépasser 4 caractères.");
        }
        if (nom.length > 8 || prenom.length > 8) {
            event.preventDefault();
            alert("Le nom et le prénom ne doivent pas dépasser 8 caractères.");
        }
    
        if (adresse.trim() === "" || adresse.length > 50) {
            event.preventDefault();
            alert("L'adresse ne doit pas être vide et ne doit pas dépasser 50 caractères.");
   
        }
        if (experience.length > 100) {
            event.preventDefault();
            alert("Le le champ experience ne doit pas deppaser 100 char .");
        }
        
        if ( (!disponible.checked ||!dispo.checked) && ( disponible.checked || dispo.checked)  ){
             event.preventDefault();
             alert("Veuillez choisir 'oui' ou 'non' pour la disponibilité.");
        }

        if (idformation.length > 4 ) {
            event.preventDefault();
            alert("Le champ id du formation ne doivent pas dépasser 4 caractères.");
        }
    });
}


function verifformation(){
    document.getElementById("formm").addEventListener("submit", function(event) {    
        var idformation = document.getElementById("id").value;
        var titre = document.getElementById("titre").value;
        var durre = document.getElementById("durre").value;
        var prix = document.getElementById("prix").value;
        var choix = document.getElementById("nom").value;
        var description = document.getElementById("description").value;

        if (idformation.length > 4 ) {
            event.preventDefault();
            alert("Le champ id du formation ne doivent pas dépasser 4 caractères.");
        }
        if (titre.length > 30 ) {
            event.preventDefault();
            alert("Le titre ne doivent pas dépasser 30 caractères.");
        }
        if (durre.length > 10 ) {
            event.preventDefault();
            alert("durre invalide.");
        }   

        if (prix.trim() === "") {
            event.preventDefault();
            alert("Laprix ne doit pas être vide.");
        }

        if (choix === "") {
            event.preventDefault();
            alert("Veuillez choisir une option dans la liste déroulante.");
        }
        

        if (description.trim() === "") {
            event.preventDefault();
            alert("La zone de texte ne doit pas être vide.");
        }
    });
}



function verifmateriel(){
    document.getElementById("formm").addEventListener("submit", function(event) {    
     var idmat = document.getElementById("nommateriel").value;
     var nommat = document.getElementById("nommat").value;
     var dispo = document.getElementById("dispo");
     var disponible = document.getElementById("disponible");
     var stock = document.getElementById("stock");
     var stcock2 = document.getElementById("stock2");
     var reserver = document.getElementById("reserver");
     var reserver2 = document.getElementById("reserver2");
     var idformation = document.getElementById("idformation").value; 
        if (idmat.length > 4 ) {
            event.preventDefault();
            alert("Le champ id du materiel ne doivent pas dépasser 4 caractères.");
        }
        if (nommat.length > 20 ) {
            event.preventDefault();
            alert("Le champ nom materiel ne doivent pas dépasser 10 caractères.");
        }
        if (!disponible.checked ||!dispo.checked) {
            event.preventDefault();
            alert("Veuillez choisir 'oui' ou 'non' pour la disponibilité.");
        }
        if (!stock.checked ||!stcock2.checked) {
            event.preventDefault();
            alert("Veuillez choisir 'oui' ou 'non' pour la stackage.");
        }
        if (!reserver.checked ||!reserver2.checked) {
            event.preventDefault();
            alert("Veuillez choisir 'oui' ou 'non' pour la reservation.");
        }
        if (idformation.length > 4 ) {
            event.preventDefault();
            alert("Le champ id duformation ne doivent pas dépasser 4 caractères.");
        }

    });
}

