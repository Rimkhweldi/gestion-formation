<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>formation</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
</head>
<body>
    <header>
       
        <nav class="navbar">
            <a href="#" class="logo"><img src="graduation-cap-png-clipart-21.png" width="70px" height="50px"> DataDoit-academy</a>
            <div class="nav-links">
                 <ul>
                     <li class ='active'><a href="#">home</a></li>
                     <li><a href="#formation">formation</a></li>
                     <li><a href="#about us">info</a></li>
                     <li><a href="#about us">contacts</a></li>
                </ul>
            </div>
        </nav>
        <div class="text-box">
           <h1>La plus grande académie de formation </h1>
           <p>Rejoignez notre site de formation dès aujourd'hui et transformez votre avenir ! Nous vous invitons à embarquer dans un voyage passionnant de croissance personnelle et de développement professionnel. Notre plateforme offre une richesse de connaissances, des cours de pointe et des ressources pédagogiques de haute qualité, le tout conçu pour vous aider à acquérir les compétences dont vous avez besoin pour réussir.</p>
            <a href="signup.html" class="hero" >join us now !</a>
        </div>
    </header>
    <section class="formation">
        <h1 id="formation">les formations qui nous offrez</h1>
       
        <div class="row">
            
                <?php  $con = mysqli_connect("localhost", "root","","data");?>
                <?php   $req = "select * from formation "; 
                    $result= $con->query($req);
                    
                    if (!$result){
                        echo 'erreur lors d execution'. $conn->connect_error;
                    }else{
                    ?>
                    <?php  while($ligne =$result->fetch_assoc()) {
                    ?>
                    <div class="formation-col">
                    <h3><?php  echo $ligne['titre']?> </h3>
                    <p><?php  echo $ligne['discription']?></p>
                    </div>
               <?php }} ?>
               
              

        </div>
    </section>

    <section class="footer">
        <h4 id="about us">about us</h4>
        <p>Notre académie offre de nombreux services dans différents domaines. Si vous souhaitez obtenir plus d'informations <br> veuillez nous contacter</p>
        <div class="icons">
            <i class="fa fa-facebook"></i>
            <i class="fa fa-twitter"></i>
            <i class="fa fa-instagram"></i>
            <i class="fa fa-linkedin"></i>
        </div>
        <p>made with <i class="fa fa-heart-o"></i> by DataDoti-academy team</p>
    </section>
        
    
</body>
</html>